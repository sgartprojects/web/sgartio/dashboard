function getHomepageGreeting(){
    let hoursNow = new Date().getHours();
    if (hoursNow >= 0 && hoursNow <= 11)
        return "Good morning"
    else if (hoursNow >= 12 && hoursNow <= 17)
        return "Good afternoon"
    else if (hoursNow >= 18 && hoursNow <= 21)
        return "Good evening"
    else if (hoursNow >= 22 && hoursNow <= 23)
        return "Good night"
    else
        return "Hey there"
}

function getUsername(){
    if (localStorage.getItem("username") === null)
        return "!";
    else
        return `, ${localStorage.getItem("username")}!`;
}

function setUsername(username){
    if (username === null){
        removeUsername();
        return;
    }
    localStorage.setItem("username", username);
}

function removeUsername(){
    localStorage.removeItem("username");
}

function executeClick (url, uptime) {
    if (uptime === "null") {
        uptime = null;
    }
    checkUptime(url, uptime);
}

function pageOffline() {
    M.toast({html: "Hey! I'm sorry but this page is currently offline.", classes: "aurora-red"});
}