const RenderDashboard = {
    data(){
        return{
            imageRoot: "./assets/icons/",
            categories: [
                {
                    title: "Themes",
                    items: [
                        { title: "Light", description: "Flashbang!", url: "javascript:setTheme('snow')", icon: "flashbang.png", uptime: null },
                        { title: "Solarized-Light", description: "Daniel", url: "javascript:setTheme('solarized-light')", icon: "daniel.png", uptime: null },
                        { title: "Dark", description: "When the night falls", url: "javascript:setTheme('polar')", icon: "moon.png", uptime: null },
                        { title: "Solarized-Dark", description: "The cooler Daniel", url: "javascript:setTheme('solarized-dark')", icon: "coolerdaniel.png", uptime: null },
                        { title: "Amoled", description: "Welcome to the dark side!", url: "javascript:setTheme('amoled')", icon: "vader.png", uptime: null }
                    ]
                },
                {
                    title: 'Public',
                    items: [
                        { title: 'Dashboard', description: 'Self written dashboard', url: 'https://dash.sgart.io', icon: 'noicon.png', uptime: "https://healthchecks.io/badge/71cf08cb-4955-46ec-9034-836660/jb8asWZG-2/dashboard.json" },
                        { title: 'Search', description: 'Customized search frontend', url: 'https://search.sgart.io', icon: 'search.png', uptime: "https://healthchecks.io/badge/71cf08cb-4955-46ec-9034-836660/EkwS3ru7-2/search.json" },
                        { title: 'Cryption Tool', description: 'De- and encrypt text', url: 'https://crypt.sgart.io', icon: 'lock.png', uptime: "https://healthchecks.io/badge/71cf08cb-4955-46ec-9034-836660/ZtpMnGJK-2/cryptiontool.json" },
                        { title: 'CDN', description: 'Content Delivery Network', url: 'https://cdn.sgart.io/', icon: 'cdn.png', uptime: "https://healthchecks.io/badge/71cf08cb-4955-46ec-9034-836660/3mqg8qeT-2/cdn.json" }
                    ]
                },
                {
                    title: "Social Media",
                    items: [
                        { title: "Twitch", description: "Streaming games n' stuff", url: 'https://twitch.tv/another_saaaga', icon: "twitch.png", uptime: null },
                        { title: "Medal TV", description: "Some clips and montages", url: 'https://medal.tv/invite/Another%20Saaaga', icon: "medal.png", uptime: null },
                        { title: "Twitter", description: "Random stuff here", url: "https://twitter.com/randomsgart", icon: "twitter.png", uptime: null },
                        { title: "Steam", description: "Games n' other things", url: "https://steamcommunity.com/id/another_saaaga/", icon: "steam.png", uptime: null },
                        { title: "Linkedin", description: "What even is this?", url: "https://linkedin.com/in/samuelgartmann", icon: "linkedin.png", uptime: null },
                        { title: "Discord", description: "Imagine a place...", url: "https://discord.sgart.io", icon: "discord.png", uptime: null }
                    ]
                },
                {
                  title: "Personal",
                  items: [
                      { title: 'Cloud', description: 'Nextcloud instance', url: 'https://cloud.sgart.io', icon: 'nextcloud.svg', uptime: "https://healthchecks.io/badge/71cf08cb-4955-46ec-9034-836660/2PdSH_mN-2/cloud.json" },
                      { title: 'Plex', description: 'Plex media server', url: 'https://plex.sgart.io', icon: 'plex.png', uptime: "https://healthchecks.io/badge/71cf08cb-4955-46ec-9034-836660/F_dC4j3f-2/plex.json" },
                      { title: 'Umami', description: 'Umami Analytics', url: 'https://umami.sgart.io', icon: 'umami.png', uptime: "https://healthchecks.io/badge/71cf08cb-4955-46ec-9034-836660/vhnoTspO-2/umami.json" }
                  ]
                },
                {
                    title: 'External',
                    items: [
                        { title: 'healthchecks.io', description: 'Uptime Tracking', url: 'https://healthchecks.io', icon: 'healthchecksio.png', uptime: null },
                        { title: 'Statuspage', description: 'Atlassian Statuspage', url: 'https://sgartio.statuspage.io', icon: 'statuspageio.png', uptime: null },
                        { title: 'Cloudflare', description: 'DNS and proxy Service', url: 'https://cloudflare.com', icon: 'cloudflare.png', uptime: null },
                        { title: 'Hostpoint', description: 'Domain Service', url: 'https://hostpoint.ch', icon: 'hostpoint.png', uptime: null },
                        { title: 'Autoeli', description: "Need some cars? Get them here!", url: 'https://autoe.li', icon: "autoeli.png", uptime: null },
                        { title: 'Fortnyan', description: "He do be dancing on nyancat", url: 'https://fortnyan.com', icon: "nyancat.png", uptime: null },
                        { title: "Sourcecode", description: "Sourcecode of this page", url: "https://gitlab.com/sgartprojects/web/dashboard", icon: "sourcecode.png", uptime: null }
                    ]
                },
                {
                    title: 'Private',
                    items: [
                        { title: 'Home Assistant', description: 'Home Assistant instance', url: 'https://home.sgart.io', icon: 'home-assistant.svg', uptime: "https://healthchecks.io/badge/71cf08cb-4955-46ec-9034-836660/bsCZSy6g-2/homeassistant.json" },
                        { title: 'Pi-Hole', description: 'Pi-Hole instance', url: 'https://pihole.sgart.io', icon: 'pi-hole.png', uptime: "https://healthchecks.io/badge/71cf08cb-4955-46ec-9034-836660/a5g9hg8W-2/pi-hole.json" },
                        { title: 'Nginx Proxy Manager', description: 'Nginx Proxy Manager', url: 'https://nginx.sgart.io', icon: 'nginxproxymanager.png', uptime: "https://healthchecks.io/badge/71cf08cb-4955-46ec-9034-836660/TYIgi6Yn-2/nginx.json" },
                        { title: 'Portainer', description: 'Docker WebUI', url: 'https://portainer.sgart.io', icon: 'portainer.png', uptime: "https://healthchecks.io/badge/71cf08cb-4955-46ec-9034-836660/IVh2OUKh-2/portainer.json" },
                        { title: 'TrueNAS', description: 'TrueNAS WebUI', url: 'https://truenas.sgart.io', icon: 'truenas.png', uptime: "https://healthchecks.io/badge/71cf08cb-4955-46ec-9034-836660/nPh5m8kp-2/truenas.json" },
                        { title: 'VPN', description: 'Wireguard WebUI', url: 'https://vpn.sgart.io', icon: 'wireguard.png', uptime: null }
                    ]
                }
            ]
        }
    }
}

Vue.createApp(RenderDashboard).mount('#dashboard');
